Name: modulemd-tools
Version: 0.13
Release: 1
Summary: Collection of tools for parsing and generating modulemd YAML files
License: MIT
BuildArch: noarch

%global release_name %{name}-%{version}-1
%global python3 %{__python3}
# This is intended for Python 3 only, hence also no Python version in the name.
%global __pytest /usr/bin/pytest%(test %{python3_pkgversion} == 3 || echo -%{python3_version})
%global pytest %{expand:\\\
  CFLAGS="${CFLAGS:-${RPM_OPT_FLAGS}}" LDFLAGS="${LDFLAGS:-${RPM_LD_FLAGS}}"\\\
  PATH="%{buildroot}%{_bindir}:$PATH"\\\
  PYTHONPATH="${PYTHONPATH:-%{buildroot}%{python3_sitearch}:%{buildroot}%{python3_sitelib}}"\\\
  PYTHONDONTWRITEBYTECODE=1\\\
  %{?__pytest_addopts:PYTEST_ADDOPTS="${PYTEST_ADDOPTS:-} %{__pytest_addopts}"}\\\
  %__pytest}

URL: https://github.com/rpm-software-management/modulemd-tools
Source0: %{url}/archive/%{release_name}.tar.gz

BuildRequires: createrepo_c
BuildRequires: python3-devel
BuildRequires: python3-pip
BuildRequires: python3-setuptools
BuildRequires: python3-wheel
BuildRequires: python3-libmodulemd >= 2.9.3
BuildRequires: python3-dnf
BuildRequires: python3-hawkey
BuildRequires: python3-createrepo_c
BuildRequires: python3-pyyaml
BuildRequires: python3-pytest
BuildRequires: python3-argparse-manpage
BuildRequires: python3-koji

Requires: createrepo_c
Requires: python3-dnf
Requires: python3-hawkey
Requires: python3-createrepo_c
Requires: python3-pyyaml
Requires: python3-libmodulemd >= 2.9.3
Requires: python3-koji


%description
Tools provided by this package:

repo2module - Takes a YUM repository on its input and creates modules.yaml
    containing YAML module definitions generated for each package.

dir2module - Generates a module YAML definition based on essential module
    information provided via command-line parameters. The packages provided by
    the module are found in a specified directory or a text file containing
    their list.

createrepo_mod - A small wrapper around createrepo_c and modifyrepo_c to provide
    an easy tool for generating module repositories.

modulemd-add-platform - Add a new context configuration for a new platform
    into a modulemd-packager file.

modulemd-merge - Merge several modules.yaml files into one. This is useful for
    example if you have several yum repositories and want to merge them into one.

modulemd-generate-macros - Generate module-build-macros SRPM package, which is
    a central piece for building modules. It should be present in the buildroot
    before any other module packages are submitted to be built.

bld2repo - Simple tool for dowloading build required RPMs of a modular build from koji.


%prep
%setup -q -n %{name}-%{release_name}


%build
cd repo2module
%py3_build
cd ..

cd dir2module
%py3_build
cd ..

cd createrepo_mod
%py3_build
cd ..

cd modulemd-add-platform
%py3_build
cd ..

cd modulemd-merge
%py3_build
cd ..

cd modulemd_tools
%py3_build
cd ..

cd bld2repo
%py3_build
cd ..

PYTHONPATH=./modulemd_tools:./bld2repo ./man/generate-manpages.sh


%install
cd repo2module
%py3_install
cd ..

cd dir2module
%py3_install
cd ..

cd createrepo_mod
%py3_install
cd ..

cd modulemd-add-platform
%py3_install
cd ..

cd modulemd-merge
%py3_install
cd ..

cd modulemd_tools
%py3_install
cd ..

cd bld2repo
%py3_install
cd ..

cp modulemd-generate-macros/modulemd-generate-macros.py \
    %{buildroot}%{_bindir}/modulemd-generate-macros

install -d %{buildroot}%{_mandir}/man1
cp man/*.1 %{buildroot}%{_mandir}/man1/


%check
export PATH=%{buildroot}%{_bindir}:$PATH

cd repo2module
%{python3} -m pytest -vv
cd ..

cd dir2module
%{python3} -m pytest -vv
cd ..

cd createrepo_mod
%{python3} -m pytest -vv
cd ..

cd modulemd-add-platform
%pytest
cd ..

cd modulemd-merge
%{python3} -m pytest -vv -s
cd ..

cd modulemd_tools
%{python3} -m pytest -vv
cd ..

cd bld2repo
%{python3} -m pytest -vv
cd ..

%files
%doc README.md
%license LICENSE
%{python3_sitelib}/repo2module
%{python3_sitelib}/repo2module-*.egg-info/
%{python3_sitelib}/dir2module
%{python3_sitelib}/dir2module-*.egg-info/
%{python3_sitelib}/createrepo_mod
%{python3_sitelib}/createrepo_mod-*.egg-info/
%{python3_sitelib}/modulemd_merge
%{python3_sitelib}/modulemd_merge-*.egg-info/
%{python3_sitelib}/modulemd_tools
%{python3_sitelib}/modulemd_tools-*.egg-info/
%{python3_sitelib}/bld2repo
%{python3_sitelib}/bld2repo-*.egg-info/
%{python3_sitelib}/modulemd_add_platform
%{python3_sitelib}/modulemd_add_platform-*.egg-info/
%{_bindir}/repo2module
%{_bindir}/dir2module
%{_bindir}/createrepo_mod
%{_bindir}/modulemd-add-platform
%{_bindir}/modulemd-merge
%{_bindir}/modulemd-generate-macros
%{_bindir}/bld2repo

%{_mandir}/man1/repo2module.1*
%{_mandir}/man1/dir2module.1*
%{_mandir}/man1/createrepo_mod.1*
%{_mandir}/man1/modulemd-add-platform.1*
%{_mandir}/man1/modulemd-merge.1*
%{_mandir}/man1/modulemd-generate-macros.1.*
%{_mandir}/man1/bld2repo.1.*


%changelog
* Thu Mar 23 2023 lichaoran <pkwarcraft@hotmail.com> - 0.13-1
- Initial package
